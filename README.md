# Breast Cancer Prediction Using Wisconsin Diagnostic Center Data

This repository provides a comprehensive solution for predicting breast cancer using the Wisconsin Diagnostic Breast Cancer (WDBC) dataset. The project involves data preprocessing, feature selection, model training, evaluation, and deployment using various machine learning algorithms.

## Table of Contents

- [Introduction](#introduction)
- [Dataset](#dataset)
- [Project Structure](#project-structure)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [License](#license)
- [Contributing](#contributing)
- [Acknowledgements](#acknowledgements)

## Introduction

Breast cancer is one of the most common cancers among women worldwide. Early detection and accurate diagnosis are crucial for effective treatment and improved survival rates. This project aims to develop a robust breast cancer prediction model using machine learning techniques on the WDBC dataset.

## Dataset

The Wisconsin Diagnostic Breast Cancer (WDBC) dataset is used in this project. The dataset contains features computed from a digitized image of a fine needle aspirate (FNA) of a breast mass. It includes information about the characteristics of the cell nuclei present in the image.

- **Number of instances:** 569
- **Number of attributes:** 30 numeric features
- **Class distribution:** 357 benign, 212 malignant

The dataset is available at the [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic)).

## Project Structure

```
breast-cancer-prediction/
├── data/
│   ├── raw/
│   │   └── wdbc.csv
│   └── processed/
├── notebooks/
│   ├── 01_data_preprocessing.ipynb
│   ├── 02_exploratory_data_analysis.ipynb
│   ├── 03_model_training.ipynb
│   └── 04_model_evaluation.ipynb
├── src/
│   ├── data_preprocessing.py
│   ├── feature_selection.py
│   ├── model_training.py
│   ├── model_evaluation.py
│   └── utils.py
├── tests/
│   ├── test_data_preprocessing.py
│   ├── test_feature_selection.py
│   ├── test_model_training.py
│   └── test_model_evaluation.py
├── .gitignore
├── LICENSE
├── README.md
└── requirements.txt
```

## Installation

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/Md-Farmanul-Haque/breast_cancer_prediction.git
    ```

2. Create and activate a virtual environment:
    ```bash
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install the required dependencies:
    ```bash
    pip install -r requirements.txt
    ```

## Usage

1. Preprocess the data:
    ```bash
    python src/data_preprocessing.py
    ```

2. Perform feature selection:
    ```bash
    python src/feature_selection.py
    ```

3. Train the model:
    ```bash
    python src/model_training.py
    ```

4. Evaluate the model:
    ```bash
    python src/model_evaluation.py
    ```

## Testing

To run the tests, execute the following command:
```bash
pytest tests/
```

## License

This project is licensed under the MIT License. See the [LICENSE]  file for more details.

## Contributing

Contributions are welcome! Please read the [CONTRIBUTING.md](CONTRIBUTING.md) file for guidelines on how to contribute to this project.

## Acknowledgements

- The [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/index.php) for providing the WDBC dataset.
- The contributors of open-source libraries like Scikit-learn, Pandas, NumPy, and Matplotlib.

---

This README file provides a comprehensive overview of the breast cancer prediction project, detailing the structure, installation steps, usage, and other essential information to make the repository professional and user-friendly.
