{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Breast Cancer Prediction: Data Preprocessing\n",
    "\n",
    "This notebook performs data preprocessing on the Wisconsin Diagnostic Breast Cancer (WDBC) dataset. The steps include loading the data, handling missing values, encoding categorical variables, scaling features, and splitting the data into training and testing sets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import necessary libraries\n",
    "import pandas as pd\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.preprocessing import StandardScaler, LabelEncoder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def load_data(filepath):\n",
    "    \"\"\"\n",
    "    Load the WDBC dataset from a CSV file.\n",
    "    \n",
    "    Parameters:\n",
    "    filepath (str): Path to the CSV file containing the dataset.\n",
    "    \n",
    "    Returns:\n",
    "    pd.DataFrame: Loaded dataset.\n",
    "    \"\"\"\n",
    "    return pd.read_csv(filepath)\n",
    "\n",
    "# Load the data\n",
    "filepath = 'data/raw/wdbc.csv'\n",
    "data = load_data(filepath)\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Handle Missing Values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def handle_missing_values(data):\n",
    "    \"\"\"\n",
    "    Handle missing values in the dataset.\n",
    "    \n",
    "    Parameters:\n",
    "    data (pd.DataFrame): The dataset with potential missing values.\n",
    "    \n",
    "    Returns:\n",
    "    pd.DataFrame: Dataset with missing values handled.\n",
    "    \"\"\"\n",
    "    return data.dropna()\n",
    "\n",
    "# Handle missing values\n",
    "data = handle_missing_values(data)\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Encode Categorical Variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def encode_categorical_variables(data):\n",
    "    \"\"\"\n",
    "    Encode categorical variables in the dataset.\n",
    "    \n",
    "    Parameters:\n",
    "    data (pd.DataFrame): The dataset with categorical variables.\n",
    "    \n",
    "    Returns:\n",
    "    pd.DataFrame: Dataset with encoded categorical variables.\n",
    "    \"\"\"\n",
    "    label_encoder = LabelEncoder()\n",
    "    data['diagnosis'] = label_encoder.fit_transform(data['diagnosis'])\n",
    "    return data\n",
    "\n",
    "# Encode categorical variables\n",
    "data = encode_categorical_variables(data)\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scale Features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def scale_features(data):\n",
    "    \"\"\"\n",
    "    Scale features in the dataset.\n",
    "    \n",
    "    Parameters:\n",
    "    data (pd.DataFrame): The dataset with features to be scaled.\n",
    "    \n",
    "    Returns:\n",
    "    pd.DataFrame: Dataset with scaled features.\n",
    "    \"\"\"\n",
    "    scaler = StandardScaler()\n",
    "    features = data.drop(columns=['id', 'diagnosis'])\n",
    "    scaled_features = scaler.fit_transform(features)\n",
    "    scaled_data = pd.DataFrame(scaled_features, columns=features.columns)\n",
    "    scaled_data['diagnosis'] = data['diagnosis'].values\n",
    "    return scaled_data\n",
    "\n",
    "# Scale features\n",
    "data = scale_features(data)\n",
    "data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Split Data into Training and Testing Sets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def split_data(data, test_size=0.2, random_state=42):\n",
    "    \"\"\"\n",
    "    Split the dataset into training and testing sets.\n",
    "    \n",
    "    Parameters:\n",
    "    data (pd.DataFrame): The dataset to be split.\n",
    "    test_size (float): Proportion of the dataset to include in the test split.\n",
    "    random_state (int): Random seed for reproducibility.\n",
    "    \n",
    "    Returns:\n",
    "    tuple: Training and testing sets (X_train, X_test, y_train, y_test).\n",
    "    \"\"\"\n",
    "    X = data.drop(columns=['diagnosis'])\n",
    "    y = data['diagnosis']\n",
    "    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)\n",
    "    return X_train, X_test, y_train, y_test\n",
    "\n",
    "# Split the data into training and testing sets\n",
    "X_train, X_test, y_train, y_test = split_data(data)\n",
    "\n",
    "# Display the shape of the training and testing sets\n",
    "X_train.shape, X_test.shape, y_train.shape, y_test.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save the Preprocessed Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the preprocessed data\n",
    "X_train.to_csv('data/processed/X_train.csv', index=False)\n",
    "X_test.to_csv('data/processed/X_test.csv', index=False)\n",
    "y_train.to_csv('data/processed/y_train.csv', index=False)\n",
    "y_test.to_csv('data/processed/y_test.csv', index=False)\n",
    "\n",
    "print(\"Data preprocessing completed and files saved.\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

