{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Breast Cancer Prediction: Model Training\n",
    "\n",
    "This notebook trains a machine learning model on the Wisconsin Diagnostic Breast Cancer (WDBC) dataset. The steps include loading the preprocessed data, training a model, evaluating its performance, and saving the trained model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import necessary libraries\n",
    "import pandas as pd\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.preprocessing import StandardScaler, LabelEncoder\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.metrics import accuracy_score, classification_report, confusion_matrix\n",
    "import joblib\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "\n",
    "# Set seaborn style\n",
    "sns.set(style=\"whitegrid\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def load_data(filepath):\n",
    "    \"\"\"\n",
    "    Load the dataset from a CSV file.\n",
    "    \n",
    "    Parameters:\n",
    "    filepath (str): Path to the CSV file containing the dataset.\n",
    "    \n",
    "    Returns:\n",
    "    pd.DataFrame: Loaded dataset.\n",
    "    \"\"\"\n",
    "    return pd.read_csv(filepath)\n",
    "\n",
    "# Load the preprocessed data\n",
    "X_train = load_data('data/processed/X_train.csv')\n",
    "X_test = load_data('data/processed/X_test.csv')\n",
    "y_train = load_data('data/processed/y_train.csv').squeeze()\n",
    "y_test = load_data('data/processed/y_test.csv').squeeze()\n",
    "X_train.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train the Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train_model(X_train, y_train):\n",
    "    \"\"\"\n",
    "    Train a RandomForestClassifier model.\n",
    "    \n",
    "    Parameters:\n",
    "    X_train (pd.DataFrame): Training features.\n",
    "    y_train (pd.Series): Training labels.\n",
    "    \n",
    "    Returns:\n",
    "    model: Trained model.\n",
    "    \"\"\"\n",
    "    model = RandomForestClassifier(random_state=42)\n",
    "    model.fit(X_train, y_train)\n",
    "    return model\n",
    "\n",
    "# Train the model\n",
    "model = train_model(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate the Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def evaluate_model(model, X_test, y_test):\n",
    "    \"\"\"\n",
    "    Evaluate the trained model on the test set.\n",
    "    \n",
    "    Parameters:\n",
    "    model: Trained model.\n",
    "    X_test (pd.DataFrame): Test features.\n",
    "    y_test (pd.Series): Test labels.\n",
    "    \n",
    "    Returns:\n",
    "    dict: Evaluation metrics.\n",
    "    \"\"\"\n",
    "    y_pred = model.predict(X_test)\n",
    "    accuracy = accuracy_score(y_test, y_pred)\n",
    "    report = classification_report(y_test, y_pred, target_names=['Benign', 'Malignant'])\n",
    "    confusion = confusion_matrix(y_test, y_pred)\n",
    "    \n",
    "    print(f'Accuracy: {accuracy:.4f}')\n",
    "    print('Classification Report:\\n', report)\n",
    "    print('Confusion Matrix:\\n', confusion)\n",
    "    \n",
    "    return {'accuracy': accuracy, 'report': report, 'confusion_matrix': confusion}\n",
    "\n",
    "# Evaluate the model\n",
    "evaluation_metrics = evaluate_model(model, X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot the Confusion Matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_confusion_matrix(confusion_matrix):\n",
    "    \"\"\"\n",
    "    Plot the confusion matrix.\n",
    "    \n",
    "    Parameters:\n",
    "    confusion_matrix (np.ndarray): Confusion matrix.\n",
    "    \"\"\"\n",
    "    plt.figure(figsize=(8, 6))\n",
    "    sns.heatmap(confusion_matrix, annot=True, fmt='d', cmap='Blues', xticklabels=['Benign', 'Malignant'], yticklabels=['Benign', 'Malignant'])\n",
    "    plt.xlabel('Predicted')\n",
    "    plt.ylabel('Actual')\n",
    "    plt.title('Confusion Matrix')\n",
    "    plt.show()\n",
    "\n",
    "# Plot the confusion matrix\n",
    "plot_confusion_matrix(evaluation_metrics['confusion_matrix'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save the Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def save_model(model, filepath):\n",
    "    \"\"\"\n",
    "    Save the trained model to a file.\n",
    "    \n",
    "    Parameters:\n",
    "    model: Trained model.\n",
    "    filepath (str): Path to the file where the model will be saved.\n",
    "    \"\"\"\n",
    "    joblib.dump(model, filepath)\n",
    "    print(f'Model saved to {filepath}')\n",
    "\n",
    "# Save the model\n",
    "save_model(model, 'models/random_forest_model.joblib')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run

