# visualization.py

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os

# Ensure the assets directory exists
assets_dir = "../assets/"
if not os.path.exists(assets_dir):
    os.makedirs(assets_dir)

# Load the data
data = pd.read_csv('../data/raw/wdbc.csv')

def save_plot(fig, filename):
    """
    Save a Matplotlib figure to the specified directory.

    Parameters:
    - fig (Matplotlib figure): The figure to save.
    - filename (str): The filename to save the figure as.
    """
    fig.savefig(f"{assets_dir}{filename}")

def plot_count(data, column):
    """
    Plot a count plot for a categorical feature.

    Parameters:
    - data (DataFrame): The dataset containing the feature.
    - column (str): The name of the feature column.
    """
    plt.figure(figsize=(8, 6))
    sns.countplot(data=data, x=column)
    plt.title(f"Count Plot of {column}")
    save_plot(plt, f"count_plot_{column}.png")
    plt.show()

def plot_heatmap(data):
    """
    Plot a heatmap of the correlation matrix.

    Parameters:
    - data (DataFrame): The dataset.
    """
    plt.figure(figsize=(16, 14))  # Increased figure size for better readability
    # Select only numeric columns
    numeric_data = data.select_dtypes(include=[float, int])
    corr_matrix = numeric_data.corr()
    heatmap = sns.heatmap(
        corr_matrix, 
        annot=True, 
        fmt='.2f', 
        cmap='coolwarm',
        annot_kws={"size": 10},  # Adjust the annotation font size
        linewidths=0.5  # Add linewidths between cells for better separation
    )
    heatmap.set_xticklabels(
        heatmap.get_xticklabels(), 
        rotation=45, 
        horizontalalignment='right',
        fontsize=12  # Adjust the font size of x-axis labels
    )
    heatmap.set_yticklabels(
        heatmap.get_yticklabels(), 
        rotation=0, 
        fontsize=12  # Adjust the font size of y-axis labels
    )
    plt.title("Heatmap of Correlation Matrix", fontsize=15)  # Adjust the title font size
    plt.tight_layout()  # Adjust the padding to prevent overlap
    save_plot(plt, "heatmap.png")
    plt.show()

'''def plot_pairplot(data, hue):
    """
    Plot pair plots of the dataset.

    Parameters:
    - data (DataFrame): The dataset.
    - hue (str): The column name to color code the plot.
    """
    sns.pairplot(data, hue=hue)
    plt.title("Pair Plots")
    save_plot(plt, "pairplot.png")
    plt.show()
'''
def plot_box(data, columns):
    """
    Plot box plots for the specified columns.

    Parameters:
    - data (DataFrame): The dataset.
    - columns (list): List of columns to plot.
    """
    for column in columns:
        plt.figure(figsize=(8, 6))
        sns.boxplot(data=data[column])
        plt.title(f"Box Plot of {column}")
        save_plot(plt, f"box_plot_{column}.png")
        plt.show()

# Example usage:
if __name__ == "__main__":
    # Plot count plot for the 'diagnosis' column
    plot_count(data, 'diagnosis')

    # Plot heatmap of the correlation matrix
    plot_heatmap(data)

    # Plot pair plots with 'diagnosis' as hue
    #plot_pairplot(data, 'diagnosis')

    # Plot box plots for the first few columns as an example
    columns_to_plot = data.columns[2:10]  # Adjust columns as necessary
    plot_box(data, columns_to_plot)
