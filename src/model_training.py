import pandas as pd
import joblib
import os
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

def load_data(file_path):
    """
    Load data from a CSV file.
    """
    return pd.read_csv(file_path)

def train_model(X_train, y_train):
    """
    Train a RandomForest model.
    """
    model = RandomForestClassifier(n_estimators=100, random_state=42)
    model.fit(X_train, y_train)
    return model

def evaluate_model(model, X_test, y_test):
    """
    Evaluate the trained model using the test data.
    """
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    report = classification_report(y_test, y_pred)
    matrix = confusion_matrix(y_test, y_pred)
    return accuracy, report, matrix

def save_model(model, filepath):
    """
    Save the trained model to a file.
    """
    # Create the directory if it does not exist
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    joblib.dump(model, filepath)

def main():
    # Load the data
    X_train = load_data('../data/processed/X_train.csv')
    X_test = load_data('../data/processed/X_test.csv')
    y_train = load_data('../data/processed/y_train.csv')
    y_test = load_data('../data/processed/y_test.csv')

    # Train the model
    model = train_model(X_train, y_train)

    # Evaluate the model
    accuracy, report, matrix = evaluate_model(model, X_test, y_test)

    # Print the results
    print(f"Accuracy: {accuracy:.4f}")
    print("Classification Report:\n", report)
    print("Confusion Matrix:\n", matrix)

    # Save the model
    save_model(model, '../models/random_forest_model.joblib')

if __name__ == "__main__":
    main()

