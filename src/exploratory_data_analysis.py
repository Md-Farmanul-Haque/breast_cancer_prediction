import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def load_data(filepath):
    """
    Load the WDBC dataset from a CSV file.
    
    Parameters:
    filepath (str): Path to the CSV file containing the dataset.
    
    Returns:
    pd.DataFrame: Loaded dataset.
    """
    return pd.read_csv(filepath)

def plot_class_distribution(data):
    """
    Plot the distribution of the target variable (diagnosis).
    
    Parameters:
    data (pd.DataFrame): The dataset with the target variable.
    """
    plt.figure(figsize=(8, 6))
    sns.countplot(x='diagnosis', data=data)
    plt.title('Class Distribution')
    plt.xlabel('Diagnosis')
    plt.ylabel('Count')
    plt.xticks(ticks=[0, 1], labels=['Benign', 'Malignant'])
    plt.show()

def plot_feature_distribution(data):
    """
    Plot the distribution of numerical features.
    
    Parameters:
    data (pd.DataFrame): The dataset with numerical features.
    """
    features = data.columns.drop(['id', 'diagnosis'])
    data_melted = pd.melt(data, id_vars='diagnosis', value_vars=features)
    plt.figure(figsize=(14, 10))
    sns.boxplot(x='variable', y='value', hue='diagnosis', data=data_melted)
    plt.title('Feature Distribution by Diagnosis')
    plt.xticks(rotation=90)
    plt.show()

def plot_correlation_matrix(data):
    """
    Plot the correlation matrix of the dataset.
    
    Parameters:
    data (pd.DataFrame): The dataset with numerical features.
    """
    features = data.columns.drop(['id', 'diagnosis'])
    corr_matrix = data[features].corr()
    plt.figure(figsize=(16, 12))
    sns.heatmap(corr_matrix, annot=True, fmt='.2f', cmap='coolwarm')
    plt.title('Correlation Matrix')
    plt.show()

def main():
    # Load the data
    filepath = '../data/raw/wdbc.csv'
    data = load_data(filepath)
    
    # Plot class distribution
    plot_class_distribution(data)
    
    # Plot feature distribution
    plot_feature_distribution(data)
    
    # Plot correlation matrix
    plot_correlation_matrix(data)
    
    print("Exploratory data analysis completed.")

if __name__ == "__main__":
    main()

