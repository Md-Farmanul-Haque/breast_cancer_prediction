import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder

def load_data(filepath):
    """
    Load the WDBC dataset from a CSV file.
    
    Parameters:
    filepath (str): Path to the CSV file containing the dataset.
    
    Returns:
    pd.DataFrame: Loaded dataset.
    """
    data = pd.read_csv(filepath)
    print(f"Data loaded successfully with shape: {data.shape}")
    return data

def handle_missing_values(data):
    """
    Handle missing values in the dataset.
    
    Parameters:
    data (pd.DataFrame): The dataset with potential missing values.
    
    Returns:
    pd.DataFrame: Dataset with missing values handled.
    """
    numeric_columns = data.select_dtypes(include=['number']).columns
    data[numeric_columns] = data[numeric_columns].fillna(data[numeric_columns].mean())
    print(f"Data after handling missing values: {data.shape}")
    return data

def encode_categorical_variables(data):
    """
    Encode categorical variables in the dataset.
    
    Parameters:
    data (pd.DataFrame): The dataset with categorical variables.
    
    Returns:
    pd.DataFrame: Dataset with encoded categorical variables.
    """
    label_encoder = LabelEncoder()
    data['diagnosis'] = label_encoder.fit_transform(data['diagnosis'])
    print(f"Data after encoding categorical variables: {data['diagnosis'].unique()}")
    return data

def scale_features(data):
    """
    Scale features in the dataset.
    
    Parameters:
    data (pd.DataFrame): The dataset with features to be scaled.
    
    Returns:
    pd.DataFrame: Dataset with scaled features.
    """
    scaler = StandardScaler()
    features = data.drop(columns=['id', 'diagnosis'])
    print(f"Features to be scaled: {features.shape}")
    
    scaled_features = scaler.fit_transform(features)
    scaled_data = pd.DataFrame(scaled_features, columns=features.columns)
    scaled_data['diagnosis'] = data['diagnosis'].values
    print(f"Data after scaling features: {scaled_data.shape}")
    
    return scaled_data

def split_data(data, test_size=0.2, random_state=42):
    """
    Split the dataset into training and testing sets.
    
    Parameters:
    data (pd.DataFrame): The dataset to be split.
    test_size (float): Proportion of the dataset to include in the test split.
    random_state (int): Random seed for reproducibility.
    
    Returns:
    tuple: Training and testing sets (X_train, X_test, y_train, y_test).
    """
    X = data.drop(columns=['diagnosis'])
    y = data['diagnosis']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    print(f"Training set size: {X_train.shape}, Testing set size: {X_test.shape}")
    return X_train, X_test, y_train, y_test

def main():
    # Load the data
    filepath = '../data/raw/wdbc.csv'
    data = load_data(filepath)
    
    # Handle missing values
    data = handle_missing_values(data)
    
    # Encode categorical variables
    data = encode_categorical_variables(data)
    
    # Scale features
    data = scale_features(data)
    
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = split_data(data)
    
    # Save the preprocessed data
    X_train.to_csv('../data/processed/X_train.csv', index=False)
    X_test.to_csv('../data/processed/X_test.csv', index=False)
    y_train.to_csv('../data/processed/y_train.csv', index=False)
    y_test.to_csv('../data/processed/y_test.csv', index=False)
    
    print("Data preprocessing completed and files saved.")

if __name__ == "__main__":
    main()
