import pandas as pd
import joblib
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

def load_data(filepath):
    """
    Load the dataset from a CSV file.
    
    Parameters:
    filepath (str): Path to the CSV file containing the dataset.
    
    Returns:
    pd.DataFrame: Loaded dataset.
    """
    return pd.read_csv(filepath)

def load_model(filepath):
    """
    Load a trained model from a file.
    
    Parameters:
    filepath (str): Path to the file containing the trained model.
    
    Returns:
    model: Loaded model.
    """
    return joblib.load(filepath)

def evaluate_model(model, X_test, y_test):
    """
    Evaluate the trained model on the test set.
    
    Parameters:
    model: Trained model.
    X_test (pd.DataFrame): Test features.
    y_test (pd.Series): Test labels.
    
    Returns:
    dict: Evaluation metrics.
    """
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    report = classification_report(y_test, y_pred, target_names=['Benign', 'Malignant'])
    confusion = confusion_matrix(y_test, y_pred)
    
    print(f'Accuracy: {accuracy:.4f}')
    print('Classification Report:\n', report)
    print('Confusion Matrix:\n', confusion)
    
    return {'accuracy': accuracy, 'report': report, 'confusion_matrix': confusion}

